/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenfinal.ENTITIES;

import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class InformeNotaFinal {
    private String nombre;
    private String Apellido;
    private LinkedList<Pruebas> pruebas;
    private int ponderadoFinal;

    public InformeNotaFinal() {
    }

    public InformeNotaFinal(String nombre, String Apellido, LinkedList<Pruebas> pruebas, int ponderadoFinal) {
        this.nombre = nombre;
        this.Apellido = Apellido;
        this.pruebas = pruebas;
        this.ponderadoFinal = ponderadoFinal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public LinkedList<Pruebas> getPruebas() {
        return pruebas;
    }

    public void setPruebas(LinkedList<Pruebas> pruebas) {
        this.pruebas = pruebas;
    }

    public int getPonderadoFinal() {
        return ponderadoFinal;
    }

    public void setPonderadoFinal(int ponderadoFinal) {
        this.ponderadoFinal = ponderadoFinal;
    }

    @Override
    public String toString() {
        return "InformeNotaFinal{" + "nombre=" + nombre + ", Apellido=" + Apellido + ", pruebas=" + pruebas + ", ponderadoFinal=" + ponderadoFinal + '}';
    }
    
    
    
}
