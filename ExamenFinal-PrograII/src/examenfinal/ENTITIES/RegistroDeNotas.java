/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenfinal.ENTITIES;

/**
 *
 * @author Fabio_2
 */
public class RegistroDeNotas {
    private int id;
    private int idAlumno;
    private int idPrueba;
    private int nota;

    public RegistroDeNotas(int id, int idAlumno, int idPrueba, int nota) {
        this.id = id;
        this.idAlumno = idAlumno;
        this.idPrueba = idPrueba;
        this.nota = nota;
    }

    public RegistroDeNotas() {
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdAlumno() {
        return idAlumno;
    }

    public void setIdAlumno(int idAlumno) {
        this.idAlumno = idAlumno;
    }

    public int getIdPrueba() {
        return idPrueba;
    }

    public void setIdPrueba(int idPrueba) {
        this.idPrueba = idPrueba;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }
    
    

    @Override
    public String toString() {
        return "RegistroDeNotas{" + "id=" + id + ", idAlumno=" + idAlumno + ", idPrueba=" + idPrueba + ", nota=" + nota + '}';
    }
    
    
}
