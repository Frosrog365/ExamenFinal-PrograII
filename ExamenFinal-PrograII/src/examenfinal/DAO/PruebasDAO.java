/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenfinal.DAO;

import examenfinal.ENTITIES.Alumnos;
import examenfinal.ENTITIES.MiError;
import examenfinal.ENTITIES.Pruebas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class PruebasDAO {

    public LinkedList<Pruebas> cargarTodo() {
        LinkedList<Pruebas> pruebas = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from pruebas";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                pruebas.add(cargarPruebas(rs));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new MiError("Problemas al cargar las pruebas, favor intente nuevamente");
        }

        return pruebas;
    }

    private Pruebas cargarPruebas(ResultSet rs) throws SQLException {
        Pruebas u = new Pruebas();
        u.setNombre(rs.getString("nombre"));
        u.setPorcentaje(rs.getInt("porcentaje"));
        u.setId(rs.getInt("id"));
        return u;
    }

    public Pruebas cargarID(int id) {
        Pruebas prueba = null;

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from pruebas where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                prueba = cargarPruebas(rs);
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar las pruebas, favor intente nuevamente");
        }

        return prueba;
    }
}
