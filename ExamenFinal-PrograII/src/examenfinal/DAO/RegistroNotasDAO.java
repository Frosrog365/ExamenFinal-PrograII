/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenfinal.DAO;

import examenfinal.ENTITIES.InformeNotaFinal;
import examenfinal.ENTITIES.MiError;
import examenfinal.ENTITIES.Pruebas;
import examenfinal.ENTITIES.RegistroDeNotas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class RegistroNotasDAO {

    public boolean insertar(RegistroDeNotas rn) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into registro_de_notas values (?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, rn.getIdAlumno());
            stmt.setInt(2, rn.getIdPrueba());
            stmt.setInt(3, rn.getNota());

            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo registrar el registro de nota, favor intente nuevamente");
        }
    }

    public LinkedList<Pruebas> cargarPruebas(int id) {
        try (Connection con = Conexion.getConexion()) {
            LinkedList<Pruebas> pruebas = new LinkedList<>();
            String sql = "select pruebas.nombre,pruebas.id, pruebas.porcentaje from alumnos, registro_de_notas, pruebas where alumnos.id = id_alumno"
                    + "and id_prueba = pruebas.id and id_alumno = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                pruebas.add(cargarPrueba(rs));
            }
            return pruebas;
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar los alumnos, favor intente nuevamente");
        }

    }

    public Pruebas cargarPrueba(ResultSet rs) throws SQLException {
        Pruebas p = new Pruebas();
        p.setId(rs.getInt("id"));
        p.setNombre(rs.getString("nombre"));
        p.setPorcentaje(rs.getInt("porcentaje"));
        return p;
    }

    private InformeNotaFinal cargarUsuario(ResultSet rs, int id) throws SQLException {
        InformeNotaFinal u = new InformeNotaFinal();
        u.setNombre(rs.getString("nombre"));
        u.setApellido(rs.getString("apellido"));
        u.setPruebas(cargarPruebas(id));
        return u;
    }

    public InformeNotaFinal cargarID(int id) {
        InformeNotaFinal informe = null;

        try (Connection con = Conexion.getConexion()) {
            String sql = "SELECT alumnos.nombre,pruebas.nombre, registro_de_notas.nota, registro_de_notas.nota * pruebas.porcentaje/100 porcentaje\n"
                    + "    FROM alumnos, pruebas, registro_de_notas\n"
                    + "    WHERE id_alumno = alumnos.id and id_prueba = pruebas.id and id_alumno = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                informe = cargarUsuario(rs,id);
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar los usuarios, favor intente nuevamente");
        }

        return informe;
    }

}
