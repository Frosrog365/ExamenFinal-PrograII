/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenfinal.DAO;

import examenfinal.ENTITIES.Alumnos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import examenfinal.ENTITIES.MiError;
import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class AlumnoDAO {

    public boolean insertar(Alumnos nuevoAlumno) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into alumnos values (?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, nuevoAlumno.getCedula());
            stmt.setString(2, nuevoAlumno.getNombre());
            stmt.setString(3, nuevoAlumno.getApellido());

            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo registrar el alumno, favor intente nuevamente");
        }
    }

    public LinkedList<Alumnos> cargarTodo() {
        LinkedList<Alumnos> alumnos = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from alumnos";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                alumnos.add(cargarUsuario(rs));
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar los alumnos, favor intente nuevamente");
        }

        return alumnos;
    }

    private Alumnos cargarUsuario(ResultSet rs) throws SQLException {
        Alumnos u = new Alumnos();
        u.setId(rs.getInt("id"));
        u.setCedula(rs.getString("cedula"));
        u.setNombre(rs.getString("nombre"));
        u.setApellido(rs.getString("apellido"));
        return u;
    }

    public Alumnos cargarID(int id) {
        Alumnos alumno = null;

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from alumnos where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                alumno = cargarUsuario(rs);
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar los usuarios, favor intente nuevamente");
        }

        return alumno;
    }

    public LinkedList<Alumnos> cargarFiltro(String filtro) {
        LinkedList<Alumnos> alumnos = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from alumnos where nombre like ? "
                    + " or apellido like ?" + "or cedula like ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, filtro + '%');
            stmt.setString(2, filtro + '%');
            stmt.setString(3, filtro + '%');
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                alumnos.add(cargarUsuario(rs));
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar los usuarios, favor intente nuevamente");
        }

        return alumnos;
    }
}
