/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenfinal.BO;

import examenfinal.DAO.PruebasDAO;
import examenfinal.ENTITIES.MiError;
import examenfinal.ENTITIES.Pruebas;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class PruebasBO {

    public LinkedList<Pruebas> cargarTodo() {
        PruebasDAO pdao = new PruebasDAO();
        return pdao.cargarTodo();
    }

    /**
     * Objetivo ???
     *
     * @param id ???
     * @return ??
     */
    public Pruebas cargarID(int id) {
        if (id <= 0) {
            throw new MiError("Favor seleccionar una prueba");
        }
        PruebasDAO pdao = new PruebasDAO();
        return pdao.cargarID(id);
    }
}
