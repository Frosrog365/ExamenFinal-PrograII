/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenfinal.BO;

import examenfinal.DAO.AlumnoDAO;
import examenfinal.DAO.Conexion;
import examenfinal.ENTITIES.Alumnos;
import examenfinal.ENTITIES.MiError;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

/**
 *
 * @author Fabio_2
 */
public class AlumnosBO {

    public boolean registrar(Alumnos nuevoAlumno) {
        //Validar el usuario, campos requeridos, igualdad de contraseñas,
        //Formatos de fechas, cedulas, numeros, etc
        if (nuevoAlumno.getNombre().isEmpty()) {
            throw new MiError("Nombre Requerido");
        }
        if (nuevoAlumno.getApellido().isEmpty()) {
            throw new MiError("Apellido Requerido");
        }

        //TODO:Formato de correo???
        if (nuevoAlumno.getCedula().isEmpty()) {
            throw new MiError("Cedula requerida");
        }

        //Llamar una capa de datos
        AlumnoDAO adao = new AlumnoDAO();
        if (nuevoAlumno.getId() == 0) {
            return adao.insertar(nuevoAlumno);
        } else {
            return false;
        }
    }

    public LinkedList<Alumnos> cargarTodo(String filtro) {
        AlumnoDAO adao = new AlumnoDAO();
        if (filtro.isEmpty()) {
            return adao.cargarTodo();
        } else {
            return adao.cargarFiltro(filtro);
        }
    }

    /**
     * Objetivo ???
     *
     * @param id ???
     * @return ??
     */
    public Alumnos cargarID(int id) {
        if (id <= 0) {
            throw new MiError("Favor seleccionar un usuario");
        }
        AlumnoDAO adao = new AlumnoDAO();
        return adao.cargarID(id);
    }
}
