/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenfinal.BO;

import examenfinal.DAO.RegistroNotasDAO;
import examenfinal.ENTITIES.MiError;
import examenfinal.ENTITIES.RegistroDeNotas;

/**
 *
 * @author Fabio_2
 */
public class RegistroNotasBO {

    public boolean registrar(RegistroDeNotas rn) {
        if (rn.getNota()==-1) {
            throw new MiError("Nota Requerida");
        }
        RegistroNotasDAO adao = new RegistroNotasDAO();
        if (rn.getId() == 0) {
            return adao.insertar(rn);
        } else {
            return false;
        }

    }
}
